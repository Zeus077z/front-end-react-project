import React from "react";
import { NavLink } from "react-router-dom";
import "./styles/Photo.css";

function PhotoCard(props){
    return (
        <div class="card image-preview">
            <img src={props.thumbnailUrl} class="card-img-top" alt="logo"/>
                <div class="card-body">
                    <h5 class="card-title">Title:</h5>
                    <p class="card-text">{props.title}</p>
                    <div class="actions">
                        <NavLink to={`/complete?url=${props.url}`} className="white-text">
                            <button type="button" className='btn btn-primary' id={props.id}>
                            Open   
                            </button>
                        </NavLink>
                        <NavLink to={`/photos?albumId=${props.id}`} className="nav-link" activeClassName="active">
                            <button type="button" className='btn btn-warning' id={props.id}>
                            Edit   
                            </button>
                        </NavLink>
                        <NavLink to={`/photos?albumId=${props.id}`} className="nav-link" activeClassName="active">
                            <button type="button" className='btn btn-danger' id={props.id}>
                            Delete   
                            </button>
                        </NavLink>
                    </div>    
                </div>
         </div>
    )
}

export default PhotoCard;