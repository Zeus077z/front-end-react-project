import React from "react";
import { useLocation } from "react-router-dom";
import queryString from 'query-string';
import "./styles/Photo.css";
import 'bootstrap/dist/css/bootstrap.min.css';

function CompletePhoto() {
  const { search } = useLocation();
  const { url } = queryString.parse(search);

  return(
    <div class="text-center bg-light image-complete">
      <br></br>
      <br></br>
      <img src={url} class="rounded mx-auto d-block img-thumbnail Responsive image" alt="logo" />
    </div> 
  );
}

export default CompletePhoto;
