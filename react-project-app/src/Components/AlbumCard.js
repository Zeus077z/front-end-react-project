import React from "react";
import { NavLink } from "react-router-dom";
import "./styles/Album.css";

function AlbumCard(props){
    return (
        <div className='card albums-preview'>
            <div className='card-body'>
                <p className='card-text'><b>Title:</b> {props.title}</p>
                <p className='card-text'>{props.description}</p>
                <div class="actions">
                    <NavLink to={`/photos?albumId=${props.id}`} className="nav-link" activeClassName="active">
                        <button type="button" className='btn btn-primary' id={props.id}>
                        Open   
                        </button>
                    </NavLink>
                    <NavLink to={`/photos?albumId=${props.id}`} className="nav-link" activeClassName="active">
                        <button type="button" className='btn btn-warning' id={props.id}>
                        Edit   
                        </button>
                    </NavLink>
                    <NavLink to={`http://localhost:8080/albums/delete/${props.id}`} className="nav-link" activeClassName="active">
                        <button type="button" className='btn btn-danger' id={props.id}>
                        Delete   
                        </button>
                    </NavLink>
                </div>              
            </div>
        </div>
    )
}

export default AlbumCard;