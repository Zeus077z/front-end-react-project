import React from "react";
import "./styles/CrudPhoto.css";

function SettingsUser(props){
    return (
        <div className='card albums-preview crud-album'>
            <div className='card-body'>
            <form>
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter name"></input>
            </div>
            <br></br>
            <div class="form-group">
                <label for="exampleInputPassword1">Last Name</label>
                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Last Name"></input>
            </div>
            <br></br>
            <div class="form-group">
                <label for="exampleInputPassword1">Email</label>
                <input type="email" class="form-control" id="exampleInputPassword1" placeholder="Email"></input>
            </div>
            <br></br>
            <div class="form-group">
                <label for="exampleInputPassword1">password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"></input>
            </div>
            <br></br>
            <div class="form-group">
                <label class="form-check-label" for="exampleCheck1">Male
                    <input type="checkbox" class="radio" id="exampleCheck1"></input>
                </label>
                <label class="form-check-label" for="exampleCheck1">Female
                    <input type="checkbox" class="radio" id="exampleCheck1"></input>
                </label>
            </div>
            <br></br>
            <button type="submit" class="btn btn-primary">Update info</button>
            </form>      
            </div>
        </div>
    )
}

export default SettingsUser;