import React, {useEffect, useState} from "react";
import { useLocation } from "react-router-dom";
import PreviewPhotoCard from "./PreviewPhotoCard";
import queryString from 'query-string';
import axios from "axios";
import 'bootstrap/dist/css/bootstrap.min.css';

function Photo() {
  const { search } = useLocation();
  const { title } = queryString.parse(search);

  const baseURL = "http://localhost:8080/photos/title/" + title + "/" + sessionStorage.getItem('UserId');
  const [photos, setPhotos] = useState(null);
  const [loadingPhotos, setLoadingPhotos] = useState(true);

  useEffect(() => {
        axios.get(baseURL).then(response => {
            setPhotos(response.data);
            setLoadingPhotos(false);
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className="container mt-4">
            <br></br>
            <br></br>
            <div className="row row-cols-md-5">
                {!loadingPhotos && photos.map(photo => {
                    return(
                        <PreviewPhotoCard
                            thumbnailUrl={photo.thumbnailUrl}
                            title={photo.title}
                            url={photo.url} />
                    ) 
                })}
            </div> 
    </div>
    )
}

export default Photo;