import React, {useEffect, useState} from "react";
// import { useParams } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import AlbumCard from "./AlbumCard";
import axios from "axios";

function Albums() {
    const baseURL = "http://localhost:8080/albums/"+sessionStorage.getItem("UserId");
    const [albums, setAlbums] = useState(null);
    const [loadingAlbums, setLoadingAlbums] = useState(true);

    useEffect(() => {
        axios.get(baseURL).then(response => {
            setAlbums(response.data);
            setLoadingAlbums(false);
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className="container mt-4">
            <br></br>
            <br></br>
            <div className="row row-cols-md-5">
                {!loadingAlbums && albums.map(album => {
                    return(
                        <AlbumCard
                            id={album.id}
                            title={album.title}
                            description={album.description} />
                    ) 
                })}
            </div>           
        </div>
    );
}

export default Albums;